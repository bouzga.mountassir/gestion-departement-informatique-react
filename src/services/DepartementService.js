import axios from 'axios';
import authHeader from './auth-header';
import { baseUrl } from './AuthService';
const API_URL_DEPARTEMENT = `${baseUrl}/departement`;
const API_URL_UTILISATEUR = `${baseUrl}/utilisateur`;


class DepartementService {

    findById(id){
        return axios.get(API_URL_DEPARTEMENT + '/'+id, { headers: authHeader() })
    }

    updateChef(idDepartement,idChef,toast){
        return axios.post(API_URL_UTILISATEUR + '/nouveauChef?idDepartement='+idDepartement+'&idChef='+idChef,
            null,{ headers: authHeader() })
            .then(response => {
                toast.show({ severity: 'success', summary: 'Opération réussie',
                    detail: 'Le chef du département est changé avec succès', life: 10000 });
            }).catch(function (error){
                toast.show({severity:'error', summary: 'Erreur', detail:error.response.data.errors[0], life: 10000});
            });
    }

    updateAdjoint(idDepartement,idAdjoint,toast){
        return axios.post(API_URL_UTILISATEUR + '/nouveauAdjoint?idDepartement='+idDepartement+'&idAdjoint='+idAdjoint,
            null,{ headers: authHeader() })
            .then(response => {
                toast.show({ severity: 'success', summary: 'Opération réussie',
                    detail: 'Le chef du département adjoint est changé avec succès', life: 10000 });
            }).catch(function (error){
                toast.show({severity:'error', summary: 'Erreur', detail:error.response.data.errors[0], life: 10000});
            });
    }

}

export default new DepartementService();
