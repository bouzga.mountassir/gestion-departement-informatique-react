import axios from "axios";
import qs from "qs";

export const baseUrl = "http://localhost:8080/GDI"
const API_URL = baseUrl;

/*defines a class named "AuthService" that contains several methods for handling user authentication.
-The login method takes in a username and password, encodes them into a JSON object and sends a POST request to the server with the encoded data.
-The logout method removes the user data from local storage.
-The register method sends a POST request to the server with a JSON object containing the username, email, and password.
-The getCurrentUser method retrieves the user data from local storage and parses it as a JSON object.
It exports an instance of the AuthService class.*/
class AuthService {
  login(username, password) {
    const data = qs.stringify({"email": username ,"password":password});
    console.log(data);
    return axios
      .post(API_URL + "/login", data)
      .then(response => {
        if (response.data.access_token) {
          console.log(response.data);
          localStorage.setItem("user", JSON.stringify(response.data));
        }
        console.log(response.data);
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(username, email, password) {
    return axios.post(API_URL + "signup", {
      username,
      email,
      password
    });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));
  }
}

export default new AuthService();
