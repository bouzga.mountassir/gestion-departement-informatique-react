import axios from 'axios';
import authHeader from './auth-header';
import { baseUrl } from './AuthService';
const API_URL = `${baseUrl}/fourniture`;
/*1. It's using the axios library to make HTTP requests to the backend.
2. It's using the authHeader() function to add the JWT to the Authorization header of the HTTP request.
3. It's using the API_URL constant to define the base URL of the backend.*/


class FournitureService {

    findAllFourniture(){
        return axios.get(API_URL + '/all', { headers: authHeader() });
    }

    findFournitureByType(type){
        return axios.get(API_URL + '/type/'+type, { headers: authHeader() });
    }

    addFourniture(fourniture,toast){
        axios.post(API_URL + '/add',fourniture ,{ headers: authHeader() })
            .then(response => {
                console.log('addUser result : ' + JSON.stringify(response));
            });
    }

    updateFourniture(fourniture,toast){
        axios.put(API_URL + '/update',fourniture ,{ headers: authHeader() })
            .then(response => {
                console.log(JSON.stringify(response));
            });
    }

    deleteFourniture(id){
        return axios.delete(API_URL + '/delete/' + id,{ headers: authHeader() })
            .then(response => {
                console.log(response);
            });
    }

}

export default new FournitureService();
