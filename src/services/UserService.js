import axios from 'axios';
import authHeader from './auth-header';
import { baseUrl } from './AuthService';
const API_URL = `${baseUrl}/utilisateur`;
/*This code defines a class called UserService which contains methods that interact with a backend API at the specified URL 'http://localhost:8080/GDI/utilisateur' to perform CRUD operations on user data.

It uses the axios library to make HTTP requests to the API, and the authHeader() function to provide an Authorization header for the requests.

The findAllUser() method makes a GET request to the API's '/all' endpoint and returns the response data.

The findById(id) method makes a GET request to the API's '/' endpoint with the specified id and returns the response data.

The findUserByName(nom) method makes a GET request to the API's '/name/' endpoint with the specified nom and returns the response data.

The addUser(user,toast) method makes a POST request to the API's '/add' endpoint with the specified user data and shows a toast message on success or failure.

The updateUser(user) method makes a PUT request to the API's '/update' endpoint with the specified user data and logs the response.

The deleteUser(id) method makes a DELETE request to the API's '/delete/' endpoint with the specified id and logs the response.

This class exports a new instance of itself, which can be imported and used by other parts of the application to interact with the API.*/

class UserService {

    findAllUser() {
        return axios.get(API_URL + '/all', {headers: authHeader()});
    }

    findById(id) {
        return axios.get(API_URL + '/' + id, {headers: authHeader()})
    }

    findUserByName(nom) {
        return axios.get(API_URL + '/name/' + nom, {headers: authHeader()});
    }

    addUser(user, toast) {
        axios.post(API_URL + '/add', user, {headers: authHeader()})
            .then(response => {
                console.log('addUser result : ' + JSON.stringify(response));
                toast.show({severity: 'success', summary: 'Successful', detail: 'Utilisateur ajouté', life: 3000});
            }).catch(function (error) {
            toast.show({severity: 'error', summary: 'Erreur', detail: error.response.data.errors[0], life: 10000});
        });
    }

    updateUser(user) {
        axios.put(API_URL + '/update', user, {headers: authHeader()})
            .then(response => {
                console.log(JSON.stringify(response));
            });
    }

    deleteUser(id) {
        return axios.delete(API_URL + '/delete/' + id, {headers: authHeader()})
            .then(response => {
                console.log(response);
            });
    }
    updatePassword(user,toast) {
        axios.post(API_URL + '/password', user, {headers: authHeader()})
            .then(response => {
                toast.show({ severity: 'success', summary: 'Successful', detail: 'Votre mot de passe est modifié', life: 3000 });
            }).catch(function (error) {
            toast.show({ severity: 'error', summary: 'Erreur', detail: 'Une erreur technique est survenue', life: 3000 });
        });
    }

}

export default new UserService();
