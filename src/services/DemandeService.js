import axios from 'axios';
import authHeader from './auth-header';
import { baseUrl } from './AuthService';
const API_URL = `${baseUrl}/demande`;
/*1. It's creating a new instance of the axios library.
2. It's setting the base URL for all the HTTP requests.
3. It's adding the Authorization header to all the HTTP requests.
4. It's defining the findAll() method that sends a GET request to /api/demand*/
class DemandeService {

    findAll(){
        return axios.get(API_URL + '/all', { headers: authHeader() });
    }

    findByDemandeur(id){
        return axios.get(API_URL + '/demandeur/' + id, { headers: authHeader() });
    }

    traiterDemande(demande,toast){
        return axios.put(API_URL + '/traiter/',demande, { headers: authHeader() }).then(response => {
            toast.show({ severity: 'success', summary: 'Opération réussie', detail: 'La demande est '
                    + demande.statutCible.toLowerCase(), life: 10000 });
        }).catch(function (error){
            toast.show({severity:'error', summary: 'Erreur', detail:error.response.data.errors[0], life: 10000});
        });
    }

    findById(id){
        return axios.get(API_URL + '/'+id, { headers: authHeader() });
    }

    askMateriel(demande,toast){
        console.log('askMateriel : ' + JSON.stringify(demande));
        return axios.post(API_URL + '/add',demande ,{ headers: authHeader() })
            .then(response => {
                toast.show({ severity: 'success', summary: 'Opération réussie', detail: 'Votre demande est enregistrée', life: 10000 });
            }).catch(function (error){
                toast.show({severity:'error', summary: 'Erreur', detail:error.response.data.errors[0], life: 10000});
            });
    }
}

export default new DemandeService();
