import React, {Component} from 'react';
import {classNames} from 'primereact/utils';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Toast} from 'primereact/toast';
import {Button} from 'primereact/button';
import {Rating} from 'primereact/rating';
import {Toolbar} from 'primereact/toolbar';
import {Dialog} from 'primereact/dialog';
import {InputText} from 'primereact/inputtext';
import '../css/DataTable.css';
import UserService from "../services/UserService";
import EventBus from "../common/EventBus";
import AuthService from "../services/AuthService";
import {InputSwitch} from "primereact/inputswitch";
import {Dropdown} from "primereact/dropdown";
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable'
import {Fieldset} from "primereact/fieldset";
import {Accordion, AccordionTab} from "primereact/accordion";
import DepartementService from "../services/DepartementService";


export class PersonnelComponent extends Component {
//declare objects
    emptyUser = {
        nom: '',
        prenom: '',
        cin: '',
        tel: '',
        numBureau: '',
        roles: [],
        email: '',
        password: '',
        grade: '',
        fonction: '',
        admin: false
    }

    emptyDepartement = {
        chef: '',
        adjoint: '',
        nouveauChef: '',
        nouveauAdjoint: ''
    }

//define constructor
    constructor(props) {
        super(props);

        this.state = {
            currentUser: AuthService.getCurrentUser(),
            isAdmin: AuthService.getCurrentUser().roles.includes("ADMINISTRATEUR"),
            submitted: false,
            globalFilter: null,
            users: null,
            user: this.emptyUser,
            selectedUsers: null,
            deleteUserDialog: false,
            deleteUsersDialog: false,
            userDialog: false,
            chefDialog: false,
            adjointDialog: false,
            departement: this.emptyDepartement
        };

        this.grades = [
            {name: 'PA', code: 'PA'},
            {name: 'PH', code: 'PH'},
            {name: 'PES', code: 'PES'},
            {name: 'PREMIER', code: 'PREMIER'},
            {name: 'DEUXIEME', code: 'DEUXIEME'}
        ];

        this.allFonction = [
            {name: 'ENSEIGNANT', code: 'ENSEIGNANT'},
            {name: 'TECHNICIEN', code: 'TECHNICIEN'}
        ];

        this.cols = [
            {field: 'nom', header: 'NOM'},
            {field: 'prenom', header: 'PRENOM'},
            {field: 'fonction', header: 'FONCTION'},
            {field: 'grade', header: 'GRADE'},
            {field: 'email', header: 'EMAIL'},
            {field: 'tel', header: 'TEL'},
            {field: 'numBureau', header: 'N° Bureau'},
            {field: 'cin', header: 'CIN'}
        ];

        this.listeEnseignant = [];
        UserService.findAllUser().then(response => {
            this.listeEnseignant = response.data
                .filter(user => (user.fonction == 'ENSEIGNANT'))
                .map(user => ({name: user.nom + ' ' + user.prenom, code: user.idUtilisateur}));
        });

        this.exportColumns = this.cols.map(col => ({title: col.header, dataKey: col.field}));
//bind functions
        this.leftToolbarTemplate = this.leftToolbarTemplate.bind(this);
        this.leftToolbarDirectionTemplate = this.leftToolbarDirectionTemplate.bind(this);
        this.rightToolbarTemplate = this.rightToolbarTemplate.bind(this);
        this.rightToolbarDirectionTemplate = this.rightToolbarDirectionTemplate.bind(this);
        this.editChef = this.editChef.bind(this);
        this.editAdjoint = this.editAdjoint.bind(this);
        this.hideChefDialog = this.hideChefDialog.bind(this);
        this.hideAdjointDialog = this.hideAdjointDialog.bind(this);
        this.saveChef = this.saveChef.bind(this);
        this.saveAdjoint = this.saveAdjoint.bind(this);
        this.onDropdownChefChange = this.onDropdownChefChange.bind(this);
        this.onDropdownAdjointChange = this.onDropdownAdjointChange.bind(this);

        this.nomBodyTemplate = this.nomBodyTemplate.bind(this);
        this.prenomBodyTemplate = this.prenomBodyTemplate.bind(this);
        this.cinBodyTemplate = this.cinBodyTemplate.bind(this);
        this.telBodyTemplate = this.telBodyTemplate.bind(this);
        this.numBureauBodyTemplate = this.numBureauBodyTemplate.bind(this);
        this.rolesBodyTemplate = this.rolesBodyTemplate.bind(this);
        this.emailBodyTemplate = this.emailBodyTemplate.bind(this);
        this.gradeBodyTemplate = this.gradeBodyTemplate.bind(this);
        this.fonctionBodyTemplate = this.fonctionBodyTemplate.bind(this);
        this.deleteSelectedUsers = this.deleteSelectedUsers.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.hideDeleteUserDialog = this.hideDeleteUserDialog.bind(this);
        this.hideDeleteUsersDialog = this.hideDeleteUsersDialog.bind(this);
        this.confirmDeleteUser = this.confirmDeleteUser.bind(this);
        this.saveUser = this.saveUser.bind(this);
        this.searchUser = this.searchUser.bind(this);
        this.updateRoles = this.updateRoles.bind(this);

        this.onDropdownGradeChange = this.onDropdownGradeChange.bind(this);
        this.onDropdownFonctionChange = this.onDropdownFonctionChange.bind(this);
        this.ratingBodyTemplate = this.ratingBodyTemplate.bind(this);
        this.statusBodyTemplate = this.statusBodyTemplate.bind(this);
        this.actionBodyTemplate = this.actionBodyTemplate.bind(this);
        this.editUser = this.editUser.bind(this);

        this.openNew = this.openNew.bind(this);
        this.hideDialog = this.hideDialog.bind(this);
        this.exportCSV = this.exportCSV.bind(this);
        this.exportPDF = this.exportPDF.bind(this);
        this.confirmDeleteSelected = this.confirmDeleteSelected.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    componentDidMount() {
        const {currentUser, isAdmin} = this.state;

        console.log('CURRENT USER : ' + JSON.stringify(currentUser))
        console.log('IS ADMIN : ' + isAdmin)

        UserService.findAllUser().then(
            response => {
                this.setState({users: response.data});
            },
            error => {
                this.setState({
                    content:
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString()
                });

                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            });

        DepartementService.findById(1).then(
            response => {
                this.setState({departement: response.data});
            });
    }

    openNew() {
        this.setState({
            user: this.emptyUser,
            submitted: false,
            userDialog: true
        });
    }

    hideDialog() {
        this.setState({
            submitted: false,
            userDialog: false
        });
    }

    hideDeleteUserDialog() {
        this.setState({deleteUserDialog: false});
    }

    hideDeleteUsersDialog() {
        this.setState({deleteUsersDialog: false});
    }

    //used for saving user in the system.

    saveUser() {
        let state = {submitted: true};

        if (this.state.user.nom.trim()
            && this.state.user.prenom.trim()
            && this.state.user.cin.trim()
            && this.state.user.tel.trim()
            && this.state.user.numBureau
            && this.state.user.email.trim()) {
            let users = [...this.state.users];
            let user = {...this.state.user};
            if (this.state.user.idUtilisateur) {
                const index = this.findIndexById(this.state.user.idUtilisateur);

                this.updateRoles(user);
                users[index] = user;
                UserService.updateUser(user);
                this.toast.show({
                    severity: 'success',
                    summary: 'Successful',
                    detail: 'Utilisateur modifié',
                    life: 3000
                });
            } else {
                user.password = "1234";
                user.roles = [{nom: 'UTILISATEUR'}];
                UserService.addUser(user, this.toast);
                users.push(user);

            }

            state = {
                ...state,
                users,
                userDialog: false,
                user
            };
        }

        this.setState(state);
    }

    updateRoles(user) {
        if (user.admin) {
            user.roles = [{nom: 'ADMINISTRATEUR'}, {nom: 'UTILISATEUR'}];
        } else {
            user.roles = [{nom: 'UTILISATEUR'}];
        }
    }

    editUser(user) {
        this.setState({
            user: {...user},
            userDialog: true
        });
    }

    confirmDeleteUser(user) {
        this.setState({
            user,
            deleteUserDialog: true
        });
    }

    deleteUser() {
        let idUserToDelete = this.state.user.idUtilisateur
        let idChef = this.state.departement.chef.idUtilisateur
        let idAdjoint = this.state.departement.adjoint.idUtilisateur
        console.log(this.state.currentUser)
        if(idUserToDelete == idChef || idUserToDelete == idAdjoint){
            this.toast.show({ severity: 'error', summary: 'Suppresion impossible', detail: 'Cette utilisateur est membre de la direction,' +
                    ' Veuillez le remplacer avant la suppression', life: 10000 });
            this.setState({
                deleteUserDialog: false
            });
        } else {
            let users = this.state.users.filter(val => val.idUtilisateur !== this.state.user.idUtilisateur);
            this.setState({
                users,
                deleteUserDialog: false,
                user: this.emptyUser
            });
            UserService.deleteUser(this.state.user.idUtilisateur);
            this.toast.show({
                severity: 'success',
                summary: 'operation réussie',
                detail: 'Utilisateur supprimé',
                life: 3000
            });
        }
    }

    findIndexById(id) {
        let index = -1;
        for (let i = 0; i < this.state.users.length; i++) {
            if (this.state.users[i].idUtilisateur === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    exportCSV() {
        this.dt.exportCSV();
    }

    exportPDF() {
        const doc = new jsPDF("p", "px", "a3");
        doc.text("Titre : La liste de personnels(enseignants et technicien)", 20, 10)
        var pdfjs = document.querySelector('#headerPDF');

        doc.autoTable(this.exportColumns, this.state.users);
        doc.save('personnel-dep-info.pdf');
    }

    confirmDeleteSelected() {
        this.setState({deleteUsersDialog: true});
    }

    deleteSelectedUsers() {
        let users = this.state.users.filter(val => !this.state.selectedUsers.includes(val));
        this.setState({
            users,
            deleteUsersDialog: false,
            selectedUsers: null
        });
        this.toast.show({
            severity: 'success',
            summary: 'Opération réussie',
            detail: 'Utilisateurs supprimés',
            life: 3000
        });
    }

    onInputChange(e, name) {
        const val = (e.target && e.target.value) || '';
        let user = {...this.state.user};
        user[`${name}`] = val;

        this.setState({user});
    }

    onDropdownGradeChange(e, name) {
        let user = {...this.state.user};
        user[`${name}`] = e.value;
        console.log('onDropDownChnage : ' + JSON.stringify(user))
        this.setState({user});
    }

    onDropdownFonctionChange(e, name) {
        let user = {...this.state.user};
        user[`${name}`] = e.value;
        console.log('onDropDownChnage : ' + JSON.stringify(user))

        if (e.value == 'ENSEIGNANT') {
            this.grades = [
                {name: 'PA', code: 'PA'},
                {name: 'PH', code: 'PH'},
                {name: 'PES', code: 'PES'}
            ];
        }
        if (e.value == 'TECHNICIEN') {
            this.grades = [
                {name: 'PREMIER', code: 'PREMIER'},
                {name: 'DEUXIEME', code: 'DEUXIEME'}
            ];
        }
        this.setState({user});
    }

    leftToolbarTemplate() {
        const {isAdmin} = this.state;
        if (isAdmin) {
            return (
                <React.Fragment>
                    <Button label="Nouveau utilisateur" icon="pi pi-plus"
                            className="p-button-raised p-button-secondary p-button-text mr-2"
                            onClick={this.openNew}/>
                    {/*<Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={this.confirmDeleteSelected}
                        disabled={!this.state.selectedUsers || !this.state.selectedUsers.length} />*/}
                </React.Fragment>
            );
        }
    }

    rightToolbarTemplate() {
        return (
            <React.Fragment>
                <Button icon="pi pi-file-excel"
                        className="mr-2 p-button-raised p-button-success p-button-text inline-block"
                        onClick={this.exportCSV}/>
                <Button icon="pi pi-file-pdf" className="p-button-raised p-button-danger p-button-text inline-block"
                        onClick={this.exportPDF}
                />
            </React.Fragment>
        )
    }

    leftToolbarDirectionTemplate() {
        const {isAdmin} = this.state;
        if (isAdmin) {
            return (
                <React.Fragment>
                    <span class="font-weight-bold mr-2">Chef du département :</span>
                    <span className="mr-2">{this.state.departement.chef.nom} {this.state.departement.chef.prenom}</span>
                    <Button icon="pi pi-user"
                            className="p-button-rounded p-button-info p-button-text"
                            tooltip="Changer le chef du département"
                            tooltipOptions={{position: 'top'}}
                            onClick={() => this.editChef()}/>
                </React.Fragment>
            );
        } else {
            return (
                <React.Fragment>
                    <span
                        class="font-weight-bold mr-2">Chef du département :</span> {this.state.departement.chef.nom} {this.state.departement.chef.prenom}
                </React.Fragment>
            );
        }
    }

    rightToolbarDirectionTemplate() {
        const {isAdmin} = this.state;
        if (isAdmin) {
            return (
                <React.Fragment>
                    <span class="font-weight-bold mr-2">Chef adjoint :</span>
                    <span
                        className="mr-2">{this.state.departement.adjoint.nom} {this.state.departement.adjoint.prenom}</span>
                    <Button icon="pi pi-user"
                            className="p-button-rounded p-button-info p-button-text"
                            tooltip="Changer le chef adjoint"
                            tooltipOptions={{position: 'top'}}
                            onClick={() => this.editAdjoint()}/>
                </React.Fragment>
            );
        } else {
            return (
                <React.Fragment>
                    <span
                        class="font-weight-bold mr-2">Chef adjoint :</span> {this.state.departement.adjoint.nom} {this.state.departement.adjoint.prenom}
                </React.Fragment>
            );
        }
    }

    hideChefDialog() {
        this.setState({
            submitted: false,
            chefDialog: false
        });
    }

    hideAdjointDialog() {
        this.setState({
            submitted: false,
            adjointDialog: false
        });
    }

    saveChef() {
        DepartementService.updateChef(1, this.state.departement.nouveauChef, this.toast);
        window.location.reload(false);
    }

    saveAdjoint() {
        DepartementService.updateAdjoint(1, this.state.departement.nouveauAdjoint, this.toast);
        window.location.reload(false);
    }

    editChef() {
        this.setState({
            chefDialog: true
        });
    }

    editAdjoint() {
        this.setState({
            adjointDialog: true
        });
    }

    onDropdownChefChange(e, name) {
        let departement = {...this.state.departement};
        departement[`${name}`].idUtilisateur = e.value;
        departement[`nouveauChef`] = e.value;
        this.setState({departement});
    }

    onDropdownAdjointChange(e, name) {
        let departement = {...this.state.departement};
        departement[`${name}`].idUtilisateur = e.value;
        departement[`nouveauAdjoint`] = e.value;
        this.setState({departement});
    }

    nomBodyTemplate(rowData) {
        return rowData.nom;
    }

    prenomBodyTemplate(rowData) {
        return rowData.prenom;
    }

    cinBodyTemplate(rowData) {
        return rowData.cin;
    }

    telBodyTemplate(rowData) {
        return rowData.tel;
    }

    numBureauBodyTemplate(rowData) {
        return rowData.numBureau;
    }

    rolesBodyTemplate(rowData) {
        return rowData.roles.map(role => " " + role.nom + " ").toString();
    }

    emailBodyTemplate(rowData) {
        return rowData.email;
    }

    gradeBodyTemplate(rowData) {
        return rowData.grade;
    }

    fonctionBodyTemplate(rowData) {
        return rowData.fonction;
    }

    ratingBodyTemplate(rowData) {
        return <Rating value={rowData.rating} readOnly cancel={false}/>;
    }

    statusBodyTemplate(rowData) {
        return <span
            className={`product-badge status-${rowData.inventoryStatus.toLowerCase()}`}>{rowData.inventoryStatus}</span>;
    }

    actionBodyTemplate(rowData) {
        const {isAdmin,currentUser} = this.state;
        return (
            <React.Fragment>
                {isAdmin && (
                    <Button icon="pi pi-pencil" className="p-button-rounded p-button-outlined p-button-success mr-2"
                            onClick={() => this.editUser(rowData)}/>
                )}
                {isAdmin && (
                    <Button icon="pi pi-trash" className="p-button-rounded p-button-outlined p-button-danger"
                            onClick={() => this.confirmDeleteUser(rowData)} disabled={currentUser.id == rowData.idUtilisateur}/>
                )}
                {!isAdmin && (
                    <Button icon="pi pi-eye" className="p-button-rounded p-button-outlined p-button-success mr-2"
                            onClick={() => this.editUser(rowData)}/>
                )}
            </React.Fragment>
        );
    }

    searchUser(event) {
        console.log(event.target.value)
        //this.setState({ globalFilter: event.target.value })
        let nom = event.target.value;
        if (nom.length == 0) {
            UserService.findAllUser()
                .then(response => {
                    this.setState({
                        users: response.data
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        } else {
            UserService.findUserByName(nom)
                .then(response => {
                    this.setState({
                        users: response.data
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }

    render() {
        const {currentUser, isAdmin} = this.state;
        const header = (
            <div className="table-header">
                <h5 className="mx-0 my-1">Personnel du département informatique </h5>
                <span className="p-input-icon-left">
                    <i className="pi pi-search"/>
                    {/*<InputText type="search" onInput={(e) => this.setState({ globalFilter: e.target.value })} placeholder="Chercher..." />*/}
                    <InputText type="search" onInput={(e) => this.searchUser(e)} placeholder="Chercher..."/>
                </span>
            </div>
        );

        const userDialogFooter = (
            <React.Fragment>
                {isAdmin && (
                    <Button label="Annuler" icon="pi pi-times" className="p-button-text" onClick={this.hideDialog}/>
                )}
                {isAdmin && (
                    <Button label="Enregistrer" icon="pi pi-check" className="p-button-text" onClick={this.saveUser}/>
                )}
                {!isAdmin && (
                    <Button label="Retour" icon="pi pi-times" className="p-button-text" onClick={this.hideDialog}/>
                )}
            </React.Fragment>
        );
        const deleteUserDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideDeleteUserDialog}/>
                <Button label="Confirmer" icon="pi pi-check" className="p-button-text" onClick={this.deleteUser}/>
            </React.Fragment>
        );
        const deleteUsersDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideDeleteUsersDialog}/>
                <Button label="Confirmer" icon="pi pi-check" className="p-button-text"
                        onClick={this.deleteSelectedUsers}/>
            </React.Fragment>
        );

        const chefDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text" onClick={this.hideChefDialog}/>
                <Button label="Modifier" icon="pi pi-check" className="p-button-text" onClick={this.saveChef}/>
            </React.Fragment>
        );

        const adjointDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text" onClick={this.hideAdjointDialog}/>
                <Button label="Modifier" icon="pi pi-check" className="p-button-text" onClick={this.saveAdjoint}/>
            </React.Fragment>
        );

        return (
            <div className="datatable-crud-demo">
                <Toast ref={(el) => this.toast = el}/>

                <div className="card">
                    <Toolbar className="mb-4" left={this.leftToolbarTemplate}
                             right={this.rightToolbarTemplate}></Toolbar>
                    <Accordion className="accordion-custom">
                        <AccordionTab header={<React.Fragment><span> Direction </span></React.Fragment>}>
                            <Toolbar className="mb-4" left={this.leftToolbarDirectionTemplate}
                                     right={this.rightToolbarDirectionTemplate}></Toolbar>
                        </AccordionTab>
                    </Accordion>

                    <DataTable ref={(el) => this.dt = el}
                               value={this.state.users}
                               selection={this.state.selectedUsers}
                               onSelectionChange={(e) => this.setState({selectedUsers: e.value})}
                               dataKey="idUtilisateur" paginator rows={4} rowsPerPageOptions={[4, 10, 25]}
                               paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                               currentPageReportTemplate="Afficher  {first} à {last} de {totalRecords} utilisateurs"
                               globalFilter={this.state.globalFilter} header={header} responsiveLayout="scroll">
                        {/*<Column selectionMode={this.state.isAdmin ? "multiple":"single"} headerStyle={{ width: '3rem' }} exportable={false}></Column>*/}
                        <Column field="nom" header="NOM" sortable style={{minWidth: '8rem'}}
                                body={this.nomBodyTemplate}></Column>
                        <Column field="prenom" header="PRENOM" sortable style={{minWidth: '8rem'}}
                                body={this.prenomBodyTemplate}></Column>
                        <Column field="cin" header="CIN" body={this.cinBodyTemplate}></Column>
                        <Column field="tel" header="TEL" body={this.telBodyTemplate}
                                style={{minWidth: '8rem'}}></Column>
                        <Column field="numBureau" header="N° Bureau" body={this.numBureauBodyTemplate}
                                style={{minWidth: '5rem'}}></Column>
                        <Column field="roles" header="ROLES" body={this.rolesBodyTemplate}
                                style={{minWidth: '15rem'}}></Column>
                        <Column field="email" header="EMAIL" body={this.emailBodyTemplate}
                                style={{minWidth: '10rem'}}></Column>
                        <Column field="grade" header="GRADE" body={this.gradeBodyTemplate}
                                style={{minWidth: '5rem'}}></Column>
                        <Column field="fonction" header="FONCTION" body={this.fonctionBodyTemplate}
                                style={{minWidth: '10rem'}}></Column>
                        <Column header="ACTIONS" body={this.actionBodyTemplate} exportable={false}
                                style={{minWidth: '8rem'}}></Column>
                    </DataTable>
                </div>

                <Dialog visible={this.state.userDialog} header="Informations de l'utilisateur" modal
                        className="p-fluid auto" footer={userDialogFooter} onHide={this.hideDialog}>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div className="field">
                                    <label htmlFor="nom">Nom
                                        {this.state.submitted && !this.state.user.nom &&
                                            <small className="p-error">(*) Obligatoire</small>}
                                        {this.state.submitted && this.state.user.nom && !/^[a-zA-Z]+$/.test(this.state.user.nom) &&
                                            <small className="p-error">Veuillez entrer uniquement des lettres</small>}
                                    </label>
                                    <InputText id="nom" value={this.state.user.nom}
                                               onChange={(e) => this.onInputChange(e, 'nom')} required autoFocus
                                               className={classNames({'p-invalid': this.state.submitted && !this.state.user.nom})}
                                               disabled={!isAdmin}/>

                                </div>
                            </td>
                            <td>
                                <div className="field">
                                    <label htmlFor="prenom">Prénom
                                        {this.state.submitted && !this.state.user.prenom &&
                                            <small className="p-error">(*) Obligatoire</small>}
                                        {this.state.submitted && this.state.user.prenom && !/^[a-zA-Z]+$/.test(this.state.user.prenom) &&
                                            <small className="p-error">Veuillez entrer uniquement des lettres</small>}
                                    </label>
                                    <InputText id="prenom" value={this.state.user.prenom}
                                               onChange={(e) => this.onInputChange(e, 'prenom')} required autoFocus
                                               className={classNames({'p-invalid': this.state.submitted && !this.state.user.prenom})}
                                               disabled={!isAdmin}/>

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className="field">
                                    <label htmlFor="cin">CIN
                                        {this.state.submitted && !this.state.user.cin &&
                                            <small className="p-error">(*) Obligatoire</small>}
                                        {this.state.submitted && this.state.user.cin && !/^[a-zA-Z0-9]+$/.test(this.state.user.cin) &&
                                            <small className="p-error">Veuillez entrer uniquement des chiffres et les
                                                lettres</small>}
                                    </label>

                                    <InputText id="cin" value={this.state.user.cin}
                                               onChange={(e) => this.onInputChange(e, 'cin')} required autoFocus
                                               className={classNames({'p-invalid': this.state.submitted && !this.state.user.cin})}
                                               disabled={!isAdmin}/>

                                </div>
                            </td>
                            <td>
                                <div className="field">
                                    <label htmlFor="tel">Téléphone
                                        {this.state.submitted && !this.state.user.tel &&
                                            <small className="p-error">(*) Obligatoire</small>}
                                        {this.state.submitted && this.state.user.tel && !/^[0-9]+$/.test(this.state.user.tel) &&
                                            <small className="p-error">Veuillez entrer uniquement des chiffres</small>}
                                    </label>
                                    <InputText id="tel" value={this.state.user.tel}
                                               onChange={(e) => this.onInputChange(e, 'tel')} required autoFocus
                                               className={classNames({'p-invalid': this.state.submitted && !this.state.user.tel})}
                                               disabled={!isAdmin}/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className="field">
                                    <label htmlFor="numBureau">N° bureau
                                        {this.state.submitted && !this.state.user.numBureau &&
                                            <small className="p-error">(*) Obligatoire</small>}
                                        {this.state.submitted && this.state.user.numBureau && !/^[0-9]+$/.test(this.state.user.numBureau) &&
                                            <small className="p-error">Veuillez entrer uniquement des chiffres</small>}
                                    </label>
                                    <InputText id="numBureau" value={this.state.user.numBureau}
                                               onChange={(e) => this.onInputChange(e, 'numBureau')} required autoFocus
                                               className={classNames({'p-invalid': this.state.submitted && !this.state.user.numBureau})}
                                               disabled={!isAdmin}/>
                                </div>
                            </td>
                            <td>
                                <div className="field">
                                    <label htmlFor="email">Email
                                        {this.state.submitted && !this.state.user.email &&
                                            <small className="p-error">(*) Obligatoire</small>}
                                        {this.state.submitted && this.state.user.email && !/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(this.state.user.email) &&
                                            <small className="p-error">Veuillez entrer un format d'email valide</small>}
                                    </label>
                                    <InputText id="email" value={this.state.user.email}
                                               onChange={(e) => this.onInputChange(e, 'email')} required autoFocus
                                               className={classNames({'p-invalid': this.state.submitted && !this.state.user.email})}
                                               disabled={!isAdmin}/>

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className="field">
                                    <label
                                        htmlFor="fonction">Fonction {this.state.submitted && !this.state.user.fonction &&
                                        <small className="p-error">*) Obligatoire</small>}</label>
                                    <Dropdown value={this.state.user.fonction}
                                              options={this.allFonction}
                                              optionValue="code"
                                              optionLabel="name"
                                              onChange={(e) => this.onDropdownFonctionChange(e, 'fonction')}
                                              placeholder="Sélectionnez une fonction"
                                              required autoFocus
                                              className={classNames({'p-invalid': this.state.submitted && !this.state.user.fonction})}
                                              disabled={!isAdmin}/>
                                </div>
                            </td>
                            <td>
                                <div className="field">
                                    <label htmlFor="grade">Grade {this.state.submitted && !this.state.user.grade &&
                                        <small className="p-error">(*) Obligatoire</small>}</label>
                                    <Dropdown id="grade" value={this.state.user.grade}
                                              options={this.grades}
                                              optionValue="code"
                                              optionLabel="name"
                                              onChange={(e) => this.onDropdownGradeChange(e, 'grade')}
                                              placeholder="Sélectionnez un grade"
                                              required autoFocus
                                              className={classNames({'p-invalid': this.state.submitted && !this.state.user.grade})}
                                              disabled={!isAdmin}/>

                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </Dialog>

                <Dialog visible={this.state.deleteUserDialog} style={{width: '450px'}} header="Confirmation" modal
                        footer={deleteUserDialogFooter} onHide={this.hideDeleteUserDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle mr-3" style={{fontSize: '2rem'}}/>
                        {this.state.user && <span>Confirmez-vous la suppresion de <b>{this.state.user.nom}</b>?</span>}
                    </div>
                </Dialog>

                <Dialog visible={this.state.deleteUsersDialog} style={{width: '450px'}} header="Confirmation" modal
                        footer={deleteUsersDialogFooter} onHide={this.hideDeleteUsersDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle mr-3" style={{fontSize: '2rem'}}/>
                        {this.state.user && <span>Confirmez-vous la suppresion des utilisateurs séléctionnés?</span>}
                    </div>
                </Dialog>

                <Dialog visible={this.state.chefDialog} header="Changer le chef du déprtement" modal
                        className="p-fluid auto" footer={chefDialogFooter} onHide={this.hideChefDialog}>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div className="field">
                                    <Dropdown value={this.state.departement.chef.idUtilisateur}
                                              options={this.listeEnseignant}
                                              optionValue="code"
                                              optionLabel="name"
                                              placeholder="Sélectionnez le nouveau chef"
                                              onChange={(e) => this.onDropdownChefChange(e, 'chef')}
                                              autoFocus/>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </Dialog>

                <Dialog visible={this.state.adjointDialog} header="Changer le chef adjoint" modal
                        className="p-fluid auto" footer={adjointDialogFooter} onHide={this.hideAdjointDialog}>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div className="field">
                                    <Dropdown value={this.state.departement.adjoint.idUtilisateur}
                                              options={this.listeEnseignant}
                                              optionValue="code"
                                              optionLabel="name"
                                              placeholder="Sélectionnez le nouveau adjoint"
                                              onChange={(e) => this.onDropdownAdjointChange(e, 'adjoint')}
                                              autoFocus/>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </Dialog>

            </div>
        );
    }
}
