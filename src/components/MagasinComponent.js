import React, {Component} from 'react';
import {TabView, TabPanel} from 'primereact/tabview';
import '../css/TabView.css';
import {MaterielComponent} from "./MaterielComponent";
import {DemandeComponent} from "./DemandeComponent";
import AuthService from "../services/AuthService";
import {MesDemandesComponent} from "./MesDemandesComponent";
import {FournitureComponent} from "./FournitureComponent";
import {StatFournitureComponent} from "./StatFourniture";

export class MagasinComponent extends Component {
//define constructor
    constructor(props) {
        super(props);
        this.state = {
            currentUser: AuthService.getCurrentUser(),
            isAdmin: AuthService.getCurrentUser().roles.includes("ADMINISTRATEUR")
        }
    }

    render() {
        const {isAdmin} = this.state;
        return (
            <div className="tabview-demo">
                <div className="card">
                    <h5>Magasin du département</h5>
                    <TabView>
                        <TabPanel header="Matériel">
                            <MaterielComponent/>
                        </TabPanel>
                        <TabPanel header="Fourniture">
                            <FournitureComponent />
                        </TabPanel>
                        <TabPanel header="Mes demandes">
                            <MesDemandesComponent/>
                        </TabPanel>
                        {isAdmin && (
                            <TabPanel header="Traitement des demandes">
                                <DemandeComponent/>
                            </TabPanel>
                        )}
                        {isAdmin && (
                            <TabPanel header="Statistiques">
                                <StatFournitureComponent/>
                            </TabPanel>
                        )}

                    </TabView>
                </div>
            </div>
        )
    }
}
