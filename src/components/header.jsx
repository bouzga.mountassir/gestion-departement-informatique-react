import React from "react";
import { Features } from "./features";



export const Header = (props) => {
  const data ={
    Features: [
      {
        icon: "fa fa-comments-o",
        title: "FICHIER CSV/PDF",
        text: "Fournir la liste des enseignants et les matériels qui leurs sont affectés (sous formats CSV et PDF) ."
      },
      {
        icon: "fa fa-bullhorn",
        title: "STATISTIQUES",
        text: "Fournir des statistiques sur la fourniture."
      },
      {
        icon: "fa fa-group",
        title: "UTILISATEUR",
        text: "Dans notre application on peut modifier,supprimer,ajouter..."
      },
      {
        icon: "fa fa-magic",
        title: "MAGASIN",
        text: "L'utilisateur peut chercher et demander au cas de besoin un materiel ou une fourniture.Puis l'administrateur peut traiter les demandes et ajouter les materiels et les fournitures voulus."
      }
    ]
  };
  return (
    <header style={{ width:'100%',margin:'auto'}} id="header">
      <div className="intro">
        <div className="overlay">
          <div className="container">
            <div className="row">
              <div className="intro-text">
                <h1>
                  {props.data ? props.data.title : "Loading"}
                  <span></span>
                </h1>
                <p>{props.data ? props.data.paragraph : "Loading"}</p>
              </div>
            </div>
            <div className="row">
            <Features data={data.Features} />
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};
