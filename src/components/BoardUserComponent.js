import React, {Component} from "react";
import {Link, Navigate, redirect} from "react-router-dom";

import UserService from "../services/UserService";
import EventBus from "../common/EventBus";
import AuthService from "../services/AuthService";
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import { Button } from 'primereact/button';
import { Toast } from 'primereact/toast';
export default class BoardUser extends Component {
    //declaring a constructor 
    constructor(props) {
        super(props);
    //bind the class methods to the current object and declares an initial state object
        this.searchUser = this.searchUser.bind(this);
        this.onChangeNomUtilisateur = this.onChangeNomUtilisateur.bind(this);
        this.updateUser = this.updateUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.accept = this.accept.bind(this);
        this.reject = this.reject.bind(this);
     //declare attributs of object
        this.state = {
            content: "",
            users: [],
            nomUtilisateur: "",
            currentIndex: -1,
            currentUser: null,
            redirect: null,
            visible: false
        };
    }
//When the component mounts, it checks if the user is logged in. If not, it redirects to the home page.
//It then sets the current user in the state.
//It then calls the findAllUser() method of the UserService class to get all the users.
    componentDidMount() {
        const currentUser = AuthService.getCurrentUser();// check if the user is logged in or not

        if (!currentUser) this.setState({redirect: "/home"});//the page is redirected to the home page
        //fetch all the user details from the UserService and set it to the state variable called users
        this.setState({currentUser: currentUser, userReady: true})
        UserService.findAllUser().then(
            response => {
                this.setState({users: response.data});
            },
            error => {
                this.setState({
                    //If an error occurs, it sets the response message in the state variable called content.
                    content:
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString()
                });
//if the status code of the response is 401 which indicates unauthorized access, it will call a logout event.
                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            });
    }
   /*
1. It's calling the findAllUser() method from the UserService class.
2. It's calling the findUserByName() method from the UserService class.
3. It's setting the state of the users property to the response data.
*/
    searchUser() {
        this.setState({
            currentUser: null,
            currentIndex: -1
        });
        let nom = this.state.nomUtilisateur;
        if (nom.length == 0) {
            UserService.findAllUser()
                .then(response => {
                    this.setState({
                        users: response.data
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        } else {
            UserService.findUserByName(nom)
                .then(response => {
                    this.setState({
                        users: response.data
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        }

    }
/*is an event handler that is triggered when the user's name is changed, it then uses a defined UserService to find all users or a specific user depending on the length of the user's name.*/
    onChangeNomUtilisateur(e) {
        let nom = e.target.value;
        this.setState({
            nomUtilisateur: nom
        });
        if (nom.length == 0) {
            UserService.findAllUser()
                .then(response => {
                    this.setState({
                        users: response.data
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        } else {
            UserService.findUserByName(nom)
                .then(response => {
                    this.setState({
                        users: response.data
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }
//update user by is
    updateUser(id) {
        console.log("ID to update : " + id);
    }
//delete user by id
    deleteUser(id) {
        console.log("ID to delete : " + id);
        UserService.deleteUser(id);
        window.location.reload(false);
    }
//eccept method used to show a message when the user confirms to delete an user.
//The life of the message is set to 3000ms
    accept() {
        this.toast.show({ severity: 'info', summary: 'Confirmé', detail: 'Utilisateur supprimé', life: 3000 });
    }
//this method used to show a message when the user cancels the deletion of an user.
    reject() {
        this.toast.show({ severity: 'warn', summary: 'Rejeté', detail: 'Suppression annulée', life: 3000 });
    }

    render() {
        if (this.state.redirect) {
            return <Navigate to={this.state.redirect}/>
        }
        const {content, users, nomUtilisateur, currentIndex, currentUser} = this.state;
        return (
            <div className="container-fluid">
                <Toast ref={(el) => this.toast = el} />
                <header className="jumbotron">
                    <h3 className="text-center">Personnel du département</h3>
                </header>

                <div>
                    <div className="col-md-8" style={{maxWidth: "100%"}}>
                        <div className="input-group mb-3">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Chercher par nom complet ou partiel"
                                value={nomUtilisateur}
                                onChange={this.onChangeNomUtilisateur}
                            />
                            <div className="input-group-append">
                                <button
                                    className="btn btn-outline-secondary"
                                    type="button"
                                    onClick={this.searchUser}
                                >
                                    Chercher
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="list row">
                        <table className="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>NOM</th>
                                <th>PRENOM</th>
                                <th>CIN</th>
                                <th>TEL</th>
                                <th>N° Bureau</th>
                                <th>ROLES</th>
                                <th>EMAIL</th>
                                <th>GRADE</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.users.map(
                                    user =>
                                        <tr key={user.idUtilisateur}>
                                            <td>{user.nom}</td>
                                            <td>{user.prenom}</td>
                                            <td>{user.cin}</td>
                                            <td>{user.tel}</td>
                                            <td>{user.numBureau}</td>
                                            <td>
                                                {user.roles.map(role => role.nom + ",")}
                                            </td>
                                            <td>{user.email}</td>
                                            <td>{user.grade}</td>
                                            <td>
                                                {/*<button className="btn btn-success"
                                                        onClick={() => this.updateUser(user.idUtilisateur)}>Modifier
                                                </button>*/}
                                                {/*<button className="btn btn-danger"
                                                        onClick={() => this.deleteUser(user.idUtilisateur)}>Supprimer
                                                </button>*/}
                                                <ConfirmDialog visible={this.state.visible} onHide={() => this.setState({ visible: false })}
                                                               message="Confimez-vous la suppression de cet utilisateur ?"
                                                               header="Confirmation" icon="pi pi-exclamation-triangle" accept={this.accept} reject={this.reject}/>
                                                <Button onClick={() => this.setState({ visible: true })}
                                                        className="p-button-danger p-button-raised p-button-rounded"
                                                        icon="pi pi-times" label="Modifier"/>
                                                <ConfirmDialog visible={this.state.visible} onHide={() => this.setState({ visible: false })}
                                                               message="Confimez-vous la suppression de cet utilisateur ?"
                                                               header="Confirmation" icon="pi pi-exclamation-triangle" accept={this.accept} reject={this.reject}/>
                                                <Button onClick={() => this.setState({ visible: true })}
                                                        className="p-button-danger p-button-raised p-button-rounded"
                                                        icon="pi pi-times" label="Supprimer"/>
                                            </td>
                                        </tr>
                                )
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}
