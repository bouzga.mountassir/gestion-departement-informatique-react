import React, {Component} from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Toast} from 'primereact/toast';
import {Button} from 'primereact/button';
import {Dialog} from 'primereact/dialog';
import '../css/DataTable.css';
import UserService from "../services/UserService";
import EventBus from "../common/EventBus";
import AuthService from "../services/AuthService";
import DemandeService from "../services/DemandeService";
import {Image} from "primereact/image";

export class MesDemandesComponent extends Component {
//declare objects
    emptyDemande = {
        type: '',
        demandeur: '',
        objet: '',
        approbateur: null,
        dateDemande: '',
        dateApprobation: '',
        statut: null,
        admin: false
    }

    emptyUser = {
        nom: '',
        prenom: ''
    }
//generate constructor
    constructor(props) {
        super(props);

        this.state = {
            currentUser: AuthService.getCurrentUser(),
            isAdmin: AuthService.getCurrentUser().roles.includes("ADMINISTRATEUR"),
            demandes: null,
            demande: this.emptyDemande,
            selectedDemandes: null,
            detailDemandeDialog: false,
            annulerDemandeDialog: false,
            user: this.emptyUser
        };

        this.selectedUserId = '';
//bind functions:thats mean <<<<<The bind() function in React is used to bind the context of a method to the corresponding class instance. It allows you to pass parameters to a function when it is invoked and helps you to maintain the scope of the executed code, keeping the this keyword in context.
        this.typeBodyTemplate = this.typeBodyTemplate.bind(this);
        this.demandeurBodyTemplate = this.demandeurBodyTemplate.bind(this);
        this.statutBodyTemplate = this.statutBodyTemplate.bind(this);
        this.dateDemandeBodyTemplate = this.dateDemandeBodyTemplate.bind(this);
        this.nomObjetBodyTemplate = this.nomObjetBodyTemplate.bind(this);
        this.hideApprouverDemandeDialog = this.hideApprouverDemandeDialog.bind(this);
        this.hideRejeterDemandeDialog = this.hideRejeterDemandeDialog.bind(this);
        this.hideDetailDemandeDialog = this.hideDetailDemandeDialog.bind(this);
        this.annulerDemande = this.annulerDemande.bind(this);
        this.confirmRejeterDemande = this.confirmRejeterDemande.bind(this);
        this.confirmAnnulerDemande = this.confirmAnnulerDemande.bind(this);
        this.detailDemande = this.detailDemande.bind(this);
        this.getUser = this.getUser.bind(this);
        this.actionBodyTemplate = this.actionBodyTemplate.bind(this);
    }
//The componentDidMount() function is a lifecycle method that is called when the component is first rendered. In this function, the current user and admin status are logged to the console. Then, a call is made to the DemandeService.findByDemandeur() function, which retrieves all demandes made by the current user. The response from this function is then logged to the console and used to update the state of the component. If an error occurs, the error message is logged to the console and the user is logged out.
    componentDidMount() {
        const {currentUser, isAdmin} = this.state;

        console.log('CURRENT USER : ' + JSON.stringify(currentUser))
        console.log('IS ADMIN : ' + isAdmin)

        DemandeService.findByDemandeur(currentUser.id).then(
            response => {
                console.log(response.data)
                this.setState({demandes: response.data});
            },
            error => {
                this.setState({
                    content:
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString()
                });

                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            });
    }
//The hideAnnulerDemandeDialog(), hideApprouverDemandeDialog(), hideRejeterDemandeDialog(), and hideDetailDemandeDialog() functions are used to close modals that are used to confirm certain actions.
    hideAnnulerDemandeDialog() {
        this.setState({annulerDemandeDialog: false});
    }

    hideApprouverDemandeDialog() {
        this.setState({approuverDemandeDialog: false});
    }

    hideRejeterDemandeDialog() {
        this.setState({rejeterDemandeDialog: false});
    }

    hideDetailDemandeDialog() {
        this.setState({detailDemandeDialog: false});
    }
//The getUser() function takes in an ID and makes a call to the UserService.findById() function to retrieve the user with that ID. The response is then logged to the console.
    getUser(id) {
        UserService.findById(id).then(function (response) {
            console.log(response.data)
            //this.setState({user: response.data});
        });
    }
//The confirmAnnulerDemande(), detailDemande(), and confirmRejeterDemande() functions are used to open modals that confirm certain actions. They take in a demande object and update the state of the component to include that object and set the appropriate dialog to be open.
    confirmAnnulerDemande(demande) {
        this.setState({
            demande,
            annulerDemandeDialog: true
        });
        console.log(demande)
    }

    detailDemande(demande) {
        this.setState({
            demande,
            detailDemandeDialog: true
        });
        console.log(demande)
    }

    confirmRejeterDemande(demande) {
        this.setState({
            demande,
            rejeterDemandeDialog: true
        });
        console.log(demande)
    }
//The annulerDemande() function is used to cancel a demande. It takes the current demande and current user from the state, creates a new object with the necessary information, and then calls the DemandeService.traiterDemande() function to cancel the demande. The demande object in the state is then updated to reflect the change.
    annulerDemande() {
        const {demande,currentUser} = this.state;
        let demandeRequest = {
            "idDemande":demande.idDemande,
            "idApprobateur" : currentUser.id,
            "statutCible": 'ANNULEE'
        };
        DemandeService.traiterDemande(demandeRequest,this.toast)
        demande.statut = 'ANNULEE'
        this.setState({
            demande: {...demande},
            annulerDemandeDialog: false
        });
    }
//The findIndexById() function takes in an ID and searches through the list of materiels in the state to find the index of the materiel with that ID.
    findIndexById(id) {
        let index = -1;
        for (let i = 0; i < this.state.materiels.length; i++) {
            if (this.state.materiels[i].idMateriel === id) {
                index = i;
                break;
            }
        }

        return index;
    }
//The typeBodyTemplate(), demandeurBodyTemplate(), statutBodyTemplate(), dateDemandeBodyTemplate(), and nomObjetBodyTemplate() functions are used to format data for display in a table. They take in a row of data and return the appropriate value for that row.
    typeBodyTemplate(rowData) {
        return rowData.type;
    }

    demandeurBodyTemplate(rowData) {
        return rowData.nomDemandeur;
    }

    statutBodyTemplate(rowData) {
        if(rowData.statut == 'ENCOURS'){
            return (
                <Image src={require("../images/inprogress.jpeg")}
                       alt="Image" width="60" height="30"/>
            );
        }
        if(rowData.statut == 'APPROUVEE'){
            return (
                <Image src={require("../images/approved.webp")}
                       alt="Image" width="60" height="30"/>
            );
        }
        if(rowData.statut == 'REJETEE'){
            return (
                <Image src={require("../images/rejected.webp")}
                       alt="Image" width="60" height="30"/>
            );
        }
        if(rowData.statut == 'ANNULEE'){
            return (
                <Image src={require("../images/cancelled.jpeg")}
                       alt="Image" width="80" height="40"/>
            );
        }
    }

    dateDemandeBodyTemplate(rowData) {
        return rowData.dateDemande;
    }

    nomObjetBodyTemplate(rowData) {
        if (rowData.materiel) {
            return rowData.materiel.type;
        } else {
            return rowData.fourniture.type;
        }
    }
//The actionBodyTemplate() function is used to display the actions that can be taken on a demande in a table. It takes in a row of data and returns a button or buttons that will trigger the appropriate action when clicked. The buttons that are returned may vary depending on whether the user is an admin.
    actionBodyTemplate(rowData) {
        const {isAdmin} = this.state;
        let canCancel = rowData.statut == 'ENCOURS'
        return (
            <React.Fragment>
                <Button icon="pi pi-eye" className="p-button-rounded p-button-outlined p-button-warning mr-2"
                        onClick={() => this.detailDemande(rowData)}
                        tooltip="Détail"
                        tooltipOptions={{position: 'left'}}/>
                {canCancel && (
                    <Button icon="pi pi-times"
                            className="p-button-rounded p-button-outlined p-button-danger mr-2"
                            onClick={() => this.confirmAnnulerDemande(rowData)}
                            tooltip="Annuler"
                            tooltipOptions={{position: 'top'}}/>
                )}
            </React.Fragment>
        );
    }

    render() {
        const {currentUser, isAdmin} = this.state;
        const header = (
            <div className="table-header">
                <h5 className="mx-0 my-1">Liste des demandes </h5>
            </div>
        );

        const annulerDemadeDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideAnnulerDemandeDialog}/>
                <Button label="Confirmer" icon="pi pi-check" className="p-button-text" onClick={this.annulerDemande}/>
            </React.Fragment>
        );

        const detailDemadeDialogFooter = (
            <React.Fragment>
                <Button label="OK" icon="pi pi-times" className="p-button-rounded p-button-secondary"
                        onClick={this.hideDetailDemandeDialog}/>
            </React.Fragment>
        );

        return (
            <div className="datatable-crud-demo">
                <Toast ref={(el) => this.toast = el}/>

                <div className="card">

                    <DataTable ref={(el) => this.dt = el}
                               value={this.state.demandes}
                               dataKey="idMateriel" paginator rows={4} rowsPerPageOptions={[4, 10, 25]}
                               paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                               currentPageReportTemplate="Afficher  {first} à {last} de {totalRecords} demandes"
                               header={header} responsiveLayout="scroll">
                        <Column field="demandeur" header="DEMANDEUR" style={{minWidth: '8rem'}}
                                body={this.demandeurBodyTemplate}></Column>
                        <Column field="statut" header="STATUT" sortable style={{minWidth: '8rem'}}
                                body={this.statutBodyTemplate}></Column>
                        <Column field="type" header="TYPE" style={{minWidth: '8rem'}}
                                body={this.typeBodyTemplate}></Column>
                        <Column field="objet" header="OBJET" style={{minWidth: '8rem'}}
                                body={this.nomObjetBodyTemplate}></Column>
                        <Column field="dateDemande" header="DATE DEMANDE" style={{minWidth: '8rem'}}
                                body={this.dateDemandeBodyTemplate}></Column>
                        <Column header="ACTIONS" body={this.actionBodyTemplate} exportable={false}
                                style={{minWidth: '8rem'}}></Column>
                    </DataTable>
                </div>

                <Dialog visible={this.state.annulerDemandeDialog} style={{width: '450px'}} header="Confirmation" modal
                        footer={annulerDemadeDialogFooter} onHide={this.hideApprouverDemandeDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle mr-3" style={{fontSize: '2rem'}}/>
                        {this.state.demande && <span>Confirmez-vous l'annulation de votre demande ?</span>}
                    </div>
                </Dialog>

                <Dialog visible={this.state.detailDemandeDialog} style={{width: '450px'}} header="Détail de la demande"
                        modal footer={detailDemadeDialogFooter} onHide={this.hideDetailDemandeDialog}>
                    {this.state.demande.statut == 'ENCOURS' &&
                        <span>Votre demande est toujours encours, son statut sera mis à jour dès sont traitement par un adminstrateur. Merci de votre patience </span>}
                    {this.state.demande.statut == 'APPROUVEE' &&
                        <span>Votre demande à été approuvée par <b>{this.state.demande.nomApprobateur} </b> le <b>{this.state.demande.dateTraitement}</b> </span>}
                    {this.state.demande.statut == 'REJETEE' &&
                        <div>
                            <div>
                                <span>
                                    Votre demande à été rejetée par <b>{this.state.demande.nomApprobateur} </b> le <b>{this.state.demande.dateTraitement}</b>
                                </span>
                            </div><br/>
                            <div>
                                <span>
                                    Pour savoir plus sur les raisons de ce rejet, veuillez contacter la personne qui à effectué le traitement sur l'email ci-dessous
                                </span>
                            </div><br/>
                            <div align="center">
                                <tr>
                                    <td align="center">
                                        <Image src={require("../images/email.jpeg")}
                                               alt="Image" width="60" height="40"/>
                                    </td>
                                    <td align="center">
                                        <b>  {this.state.demande.approbateur.email}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <Image src={require("../images/bureau.jpeg")}
                                               alt="Image" width="60" height="40"/>
                                    </td>
                                    <td align="center">
                                        <b>  Bureau N° {this.state.demande.approbateur.numBureau}</b>
                                    </td>
                                </tr>
                            </div>
                        </div>
                    }
                    {this.state.demande.statut == 'ANNULEE' &&
                        <span>Vous avez annulé cette demande le <b>{this.state.demande.dateTraitement}</b> </span>}
                </Dialog>

            </div>
        );
    }
}
