import React, {Component} from "react";
import {Navigate} from "react-router-dom";
import AuthService from "../services/AuthService";
import UserService from "../services/UserService";
import EventBus from "../common/EventBus";
import {Toolbar} from "primereact/toolbar";
import {Button} from "primereact/button";
import {Dialog} from "primereact/dialog";
import {InputText} from "primereact/inputtext";
import {classNames} from "primereact/utils";
import {Password} from "primereact/password";
import {Toast} from "primereact/toast";

export default class Profile extends Component {
    emptyUser = {
        idUtilisateur: '',
        nom: '',
        prenom: '',
        cin: '',
        tel: '',
        numBureau: '',
        email: '',
        password: ''
    }

    constructor(props) {
        super(props);

        this.state = {
            redirect: null,
            userReady: false,
            currentUser: {username: ""}
        };
        this.leftToolbarTemplate = this.leftToolbarTemplate.bind(this);
        this.rightToolbarTemplate = this.rightToolbarTemplate.bind(this);
        this.editProfil = this.editProfil.bind(this);
        this.changePassword = this.changePassword.bind(this);
        this.saveUser = this.saveUser.bind(this);
        this.hideDialog = this.hideDialog.bind(this);
        this.hidePasswordDialog = this.hidePasswordDialog.bind(this);
        this.savePassword = this.savePassword.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    /*this function It retrieves the current user from AuthService
    If the current user is not found, it updates the state to redirect the user to the "/home" path
    If the current user is found, it updates the state with the current user and sets the userReady state to true*/
    componentDidMount() {
        const currentUser = AuthService.getCurrentUser();

        if (!currentUser) this.setState({redirect: "/home"});
        this.setState({
            currentUser: currentUser,
            submitted: false,
            globalFilter: null,
            userReady: true,
            user: this.emptyUser,
            userDialog: false,
            passwordDialog: false,
            confirmPassword: '',
            newPassword:''
        })
        UserService.findById(currentUser.id).then(
            response => {
                this.setState({user: response.data});
            },
            error => {
                this.setState({
                    content:
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString()
                });

                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            });
    }

    leftToolbarTemplate() {
        const {currentUser,user} = this.state;
        return (
            <React.Fragment>
                <span className="mr-2">Bienvenue </span>
                <span className="font-weight-bold mr-2">{currentUser.nom} {currentUser.prenom}</span>
            </React.Fragment>
        );
    }

    rightToolbarTemplate() {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil"
                        className="mr-2 p-button-raised p-button-success p-button-text inline-block"
                        tooltip="Modifier votre profile"
                        tooltipOptions={{position: 'top'}}
                        onClick={this.editProfil}/>
                <Button icon="pi pi-key"
                        className="p-button-raised p-button-danger p-button-text inline-block"
                        tooltip="Modifier password"
                        tooltipOptions={{position: 'top'}}
                        onClick={this.changePassword}
                />
            </React.Fragment>
        )
    }

    editProfil() {
        this.setState({
            userDialog: true
        });
    }

    changePassword() {
        this.setState({
            passwordDialog: true
        });
    }

    hideDialog() {
        this.setState({
            userDialog: false
        });
    }

    saveUser() {
        const {currentUser,user} = this.state;
        console.log(user)
        UserService.updateUser(user);
        this.toast.show({ severity: 'success', summary: 'Successful', detail: 'Vos informations sont mis à jour avec succès', life: 3000 });
        this.setState({
            userDialog: false
        });

    }

    onInputChange(e, name) {
        const val = (e.target && e.target.value) || '';
        let user = {...this.state.user};
        user[`${name}`] = val;

        this.setState({ user });
    }

    hidePasswordDialog(){
        this.setState({
            passwordDialog: false
        });
    }

    savePassword(){
        if(this.state.newPassword == this.state.confirmPassword){
            const {currentUser,user} = this.state;
            user.idUtilisateur = currentUser.id
            user.password = this.state.newPassword
            console.log(user)
            UserService.updatePassword(user,this.toast);
            this.setState({
                passwordDialog: false
            });
        } else {
            this.toast.show({severity:'error', summary: 'Erreur', detail:'Erreur de confirmation du mot de passe', life: 10000});
        }
    }

    render() {
        if (this.state.redirect) {
            return <Navigate to={this.state.redirect}/>
        }

        const {currentUser} = this.state;

        const userDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text" onClick={this.hideDialog} />
                <Button label="Enregistrer" icon="pi pi-check" className="p-button-text" onClick={this.saveUser} />
            </React.Fragment>
        );

        const passwordDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text" onClick={this.hidePasswordDialog} />
                <Button label="Modifier" icon="pi pi-check" className="p-button-text" onClick={this.savePassword} />
            </React.Fragment>
        );

        return (
            <div>
                <Toast ref={(el) => this.toast = el} />
                {(this.state.userReady) ?
                    <div>
                        <div className="card">
                            <Toolbar className="mb-4" left={this.leftToolbarTemplate}
                                     right={this.rightToolbarTemplate}></Toolbar>
                        </div>
                        <Dialog visible={this.state.userDialog} header="Vos informations personnelles" modal className="p-fluid auto" footer={userDialogFooter} onHide={this.hideDialog}>
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        <div className="field">
                                            <label htmlFor="nom">Nom {this.state.submitted && !this.state.user.nom && <small className="p-error">(*) Obligatoire</small>}</label>
                                            <InputText id="nom" value={this.state.user.nom} onChange={(e) => this.onInputChange(e, 'nom')} required autoFocus className={classNames({ 'p-invalid': this.state.submitted && !this.state.user.nom })} />

                                        </div>
                                    </td>
                                    <td>
                                        <div className="field">
                                            <label htmlFor="prenom">Prénom {this.state.submitted && !this.state.user.prenom && <small className="p-error">(*) Obligatoire</small>}</label>
                                            <InputText id="prenom" value={this.state.user.prenom} onChange={(e) => this.onInputChange(e, 'prenom')} required autoFocus className={classNames({ 'p-invalid': this.state.submitted && !this.state.user.prenom })} />

                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="field">
                                            <label htmlFor="cin">CIN {this.state.submitted && !this.state.user.cin && <small className="p-error">(*) Obligatoire</small>}</label>
                                            <InputText id="cin" value={this.state.user.cin} onChange={(e) => this.onInputChange(e, 'cin')} required autoFocus className={classNames({ 'p-invalid': this.state.submitted && !this.state.user.cin })} />

                                        </div>
                                    </td>
                                    <td>
                                        <div className="field">
                                            <label htmlFor="tel">Téléphone {this.state.submitted && !this.state.user.tel && <small className="p-error">(*) Obligatoire</small>}</label>
                                            <InputText id="tel" value={this.state.user.tel} onChange={(e) => this.onInputChange(e, 'tel')} required autoFocus className={classNames({ 'p-invalid': this.state.submitted && !this.state.user.tel })} />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="field">
                                            <label htmlFor="numBureau">N° bureau {this.state.submitted && !this.state.user.numBureau && <small className="p-error">(*) Obligatoire</small>}</label>
                                            <InputText id="numBureau" value={this.state.user.numBureau} onChange={(e) => this.onInputChange(e, 'numBureau')} required autoFocus className={classNames({ 'p-invalid': this.state.submitted && !this.state.user.numBureau })} />
                                        </div>
                                    </td>
                                    <td>
                                        <div className="field">
                                            <label htmlFor="email">Email {this.state.submitted && !this.state.user.email && <small className="p-error">(*) Obligatoire</small>}</label>
                                            <InputText id="email" value={this.state.user.email} onChange={(e) => this.onInputChange(e, 'email')} required autoFocus className={classNames({ 'p-invalid': this.state.submitted && !this.state.user.email })} />

                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </Dialog>
                        <Dialog visible={this.state.passwordDialog} header="Mot de passe" modal className="p-fluid auto" footer={passwordDialogFooter} onHide={this.hidePasswordDialog}>
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        <div className="field">
                                            <label htmlFor="type">Nouveau mot de passe</label>
                                            <Password value={this.state.newPassword} onChange={(e) => this.setState({ newPassword: e.target.value })} toggleMask />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="field">
                                            <label htmlFor="confirmPassword">Confirmer le mot de passe</label>
                                            <Password value={this.state.confirmPassword} onChange={(e) => this.setState({ confirmPassword: e.target.value })} toggleMask />
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </Dialog>

                    </div> : null}
            </div>
        );
    }
}
