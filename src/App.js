import React, { Component } from "react";
import { Routes, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import 'primereact/resources/primereact.css';

import "./App.css";

import AuthService from "./services/AuthService";

import Login from "./components/LoginComponent";
import InscriptionComponent from "./components/InscriptionComponent";
import Home from "./components/HomeComponent";
import Profile from "./components/ProfileComponent";
import BoardUser from "./components/BoardUserComponent";

// import AuthVerify from "./common/auth-verify";
import EventBus from "./common/EventBus";
import {PersonnelComponent} from "./components/PersonnelComponent";
import {MagasinComponent} from "./components/MagasinComponent";

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);
    this.toggle = this.toggle.bind(this);
    this.state = {
      showUserBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    console.log('USER CONNECTED : '+user);
    if (user) {
      this.setState({
        currentUser: user,
        showUserBoard: user.roles.includes("UTILISATEUR"),
        showAdminBoard: user.roles.includes("ADMINISTRATEUR"),
      });
    }

    EventBus.on("logout", () => {
      this.logOut();
    });
  }

  componentWillUnmount() {
    EventBus.remove("logout");
  }

  logOut() {
    AuthService.logout();
    this.setState({
      showUserBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
    });
  }

  render() {
    const { currentUser, showUserBoard, showAdminBoard } = this.state;

    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <a href="http://fso.ump.ma/fr/informatique" className="logo pull-right">
            <img src="http://fso.ump.ma/assets/logo.svg" alt="Logo UMP" height="41" className="logo-svg"/>
              <img src="http://fso.ump.ma/assets/title.svg" width="160" className="logo-title" alt=""/>
          </a>

          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/home"} className="nav-link">
                Accueil
              </Link>
            </li>
            {currentUser && (
              <li className="nav-item">
                <Link to={"/personnel"} className="nav-link">
                  Personnel
                </Link>
              </li>
            )}
            {currentUser && (
                <li className="nav-item">
                  <Link to={"/magasin"} className="nav-link">
                    Magasin
                  </Link>
                </li>
            )}
          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  {currentUser.prenom} {currentUser.nom}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="nav-link" onClick={this.logOut}>
                  Déconnexion
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Connexion
                </Link>
              </li>

             
            </div>
          )}
        </nav>

        <div  style={{ width:'100%'}}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/inscription" element={<InscriptionComponent />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/user" element={<BoardUser />} />
            <Route path="/personnel" element={<PersonnelComponent />} />
            <Route path="/magasin" element={<MagasinComponent />} />
          </Routes>
        </div>

        {/* <AuthVerify logOut={this.logOut}/> */}
      </div>
    );
  }
}

export default App;
